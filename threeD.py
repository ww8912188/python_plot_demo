import numpy as np
import math
import sys
import random
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
import time

DOTCLOUND_FILE = './data/dotCloud.csv'
SPECTRUM_FILE = './data/two_d.csv'


def findConvexHull(points):
    # 获取convex index
    hull = ConvexHull(points)
    return hull.vertices


def in_hull(p, hull):
    """
    Test if points in `p` are in `hull`

    `p` should be a `NxK` coordinates of `N` points in `K` dimensions
    `hull` is either a scipy.spatial.Delaunay object or the `MxK` array of the
    coordinates of `M` points in `K`dimensions for which Delaunay triangulation
    will be computed
    """
    if not isinstance(hull, Delaunay):
        hull = Delaunay(hull)

    return hull.find_simplex(p) >= 0


def make_ax(grid=False):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.grid(grid)
    return ax


def explode(data):
    shape_arr = np.array(data.shape)
    size = shape_arr[:3]*2 - 1
    exploded = np.zeros(np.concatenate(
        [size, shape_arr[3:]]), dtype=data.dtype)
    exploded[::2, ::2, ::2] = data
    return exploded


def expand_coordinates(indices):
    x, y, z = indices
    x[1::2, :, :] += 1
    y[:, 1::2, :] += 1
    z[:, :, 1::2] += 1
    return x, y, z


def getVec(fileName):
    vec = []
    timestamp = []
    fid = open(fileName, 'r')
    lines = fid.readlines()
    for line in lines:
        temp = line.strip().split(',')
        vec.append(temp[1:])
        timestamp.append(temp[0])
    # 去掉header
    return vec[1:], timestamp[1:]


def overlay(a, b):
    # a is the matrix to be overlayed
    # a is the small matrix
    mask = np.array(a, dtype=bool)
    mask = (mask == False)
    np.putmask(a, mask, b)
    return a


def vec2NyArr(vec, dim):
    [dimX, dimY, dimZ] = dim
    deltaX = np.max(vec[:, 0])/dimX
    deltaY = np.max(vec[:, 1])/dimY
    deltaZ = np.max(vec[:, 2])/dimZ
    arr3D = np.zeros(dim)
    for (idxRow, row) in enumerate(vec):
        [x, y, z] = row
        xIdx = int(x/deltaX)
        yIdx = int(y/deltaY)
        zIdx = int(z/deltaZ)
        if xIdx >= dimX:
            xIdx = dimX-1
        if yIdx >= dimY:
            yIdx = dimY-1
        if zIdx >= dimZ:
            zIdx = dimZ-1
        arr3D[xIdx, yIdx, :zIdx] = 1
    return arr3D


def helper1(r, c):
    v = []
    for i in range(r):
        for j in range(c):
            v.append([i, j])
    return np.array(v)


def genColor(number_of_colors):
    color = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    return color


def resize(arr, dim):
    [x_origin, y_origin, z_orgin] = arr.shape
    [x_aim, y_aim, z_aim] = dim
    resizedArr = np.zeros((x_aim, y_aim, z_aim))
    deltaX = int(x_origin/x_aim)
    deltaY = int(x_origin/y_aim)
    deltaZ = int(x_origin/z_aim)
    for i in range(x_aim-1):
        for j in range(y_aim-1):
            for k in range(z_aim-1):
                currentSlice = arr[i:(i+1)*deltaX, j:(j+1)
                                   * deltaY, k:(k+1)*deltaZ]
                [rr, cc, ll] = currentSlice.shape
                resizedArr[i, j, k] = round(
                    np.sum(currentSlice[:])/(rr*cc*ll))
    return resizedArr


def refactor(arr, dim):
    [dimX, dimY, dimZ] = dim
    q = helper1(dimX, dimY)

    for i in range(dimZ):
        # 50 x 50 matrix, z layer
        row = arr[:, :, i]
        if (np.sum(row[:]) > 2):
            indices = np.transpose(np.nonzero(row))
            checker = in_hull(q, indices)
            convex = np.reshape(checker, (dimX, dimY)).astype(int)
            arr[:, :, i] = convex
    return arr


def getPlotData(matrix, layer, dotPerLayer, dim):
    ret = []
    for i in range(layer):
        sliceArr = matrix[i*dotPerLayer:(i+1)*dotPerLayer, :]
        arr = vec2NyArr(sliceArr, dim)
        convex = refactor(arr, dim)
        convex[convex == 1] = i+1
        ret.append(convex)

    result = ret[0]
    for i in range(1, len(ret)):
        toMerge = ret[i]
        result = overlay(result, toMerge)

    return result


def do_plot(result, deDim, spectrumArr, analysis):
    # # de-sample采样精度
    # result = resize(result, deDim)
    [s1, s2, s3] = deDim
    colors = ['#D22F9A', '#41E3AD', '#67EB14', '#342699', '#3B4E99', '#E375EF', '#979912', '#05AEFB', '#0458C1', '#60D345', '#FFE66F', '#62300A', '#7635AE', '#2FA6EC', '#30FD87', '#2B025C', '#460CAC', '#E01921', '#597FEC', '#04A6E4', '#BE5981', '#83D8ED', '#E72BA4', '#90E859', '#81B0F3',
              '#B29CE6', '#46F4DF', '#68005C', '#4A1560', '#AEFEEB', '#426899', '#8E7728', '#00761A', '#DDAE30', '#A88C32', '#D9C8FB', '#AB54A5', '#B5CA8D', '#87EDBD', '#64119E', '#FFED3C', '#F97D79', '#8C5B0F', '#26A12A', '#DBBF79', '#EC9C05', '#FB3CA4', '#6874A9', '#697818', '#32B9FA']
    facecolors = np.array([[['#1f77b430']*s3]*s2]*s1)
    maxValAfterDeSample = int(np.max(result))
    for i in range(1, maxValAfterDeSample+1):
        facecolors[result == i] = colors[i]

    filled = explode(result)
    facecolors = explode(facecolors)
    x, y, z = expand_coordinates(np.indices(np.array(filled.shape) + 1))
    print('start plotting...')
    ax = make_ax()
    ax.voxels(x, y, z, filled, facecolors=facecolors, edgecolors='gray')
    [rr, _] = spectrumArr.shape
    patches = [mpatches.Patch(color=colors[i+1], label="M1 {}%, M2 {}%, M3 {}% M4 {}% M5 {}%".format(
        round(spectrumArr[i, 0], 2),
        round(spectrumArr[i, 1], 2),
        round(spectrumArr[i, 2], 2),
        round(spectrumArr[i, 3], 2),
        round(spectrumArr[i, 4], 2)
    ),) for i in range(rr)]
    patches.append(mpatches.Patch(color='black', label="ALL M1 {}%, M2 {}%, M3 {}% M4 {}% M5 {}%".format(
        round(np.mean(spectrumArr[:, 0]), 2),
        round(np.mean(spectrumArr[:, 1]), 2),
        round(np.mean(spectrumArr[:, 2]), 2),
        round(np.mean(spectrumArr[:, 3]), 2),
        round(np.mean(spectrumArr[:, 4]), 2),
    ),))
    if analysis != 'off':
        ax.legend(loc='upper left', handles=patches)
    plt.show()


def readSpectrum(layer):
    fid = open(SPECTRUM_FILE, 'r')
    lines = fid.readlines()
    # 因为点云图只有18层，频谱仪数据暂时选择6行
    r = len(lines)
    c = 5
    pic = np.zeros((r, c))
    cnt = 0
    ret = []
    for line in lines:
        content = line.strip().split(',')
        pic[cnt, :] = [content[4], content[5],
                       content[6], content[7], content[8]]
        cnt += 1
    ret = np.zeros((layer, c))
    for i in range(layer):
        sliceArr = pic[i*5:(i+1)*5, :]
        ret[i, :] = [np.mean(sliceArr[:, 0]), np.mean(sliceArr[:, 1]), np.mean(
            sliceArr[:, 2]), np.mean(sliceArr[:, 3]), np.mean(sliceArr[:, 4])]
    ret[0, :] = pic[0, :]
    return ret


def main():
    argsNum = len(sys.argv)
    if (argsNum == 2):
        timestamp = sys.argv[1]
        section = 'all'
        analysis = 'off'
    elif argsNum == 3:
        timestamp = sys.argv[1]
        section = int(sys.argv[2])
        analysis = 'off'
    elif argsNum == 4:
        timestamp = sys.argv[1]
        section = int(sys.argv[2])
        analysis = sys.argv[3]
    else:
        timestamp = 'all'
        section = 'all'
        analysis = 'off'

    # 原始数据分割单元精度 X-Y-Z
    dim = (100, 500, 100)
    layer = 18
    dotPerLayer = 100

    vec, time = getVec(DOTCLOUND_FILE)
    if timestamp != 'all':
        print('select timestamp: ' + timestamp)
        for i in range(len(time)):
            if time[i] == timestamp:
                break
        layer = int(i/100)+1
        vec = vec[:layer*100]

    matrix = np.array(vec).astype(np.float)
    data = getPlotData(matrix, layer, dotPerLayer, dim)

    if section != 'all':
        print('select section: ' + str(section))
        if section >= dim[0]:
            raise ValueError(
                'select section is bigger than original X dimension ' + str(dim[0]))
        data = data[section:, :, :]
        [newX, _, _] = data.shape
        dim = (newX, 20, 20)

    spectrum_data = readSpectrum(layer)

    # do_plot(data, dim, spectrum_data, analysis)


if __name__ == '__main__':
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
