# python_plot_demo


### how to start
1. install venv
```bash
python3 -m venv venv
```
2. start venv
```bash
source ./venv/bin/activate
```

3. install requirements
```bash
pip install -r requirements.txt
```

4. start plotting
```
# plot ./data/dotCloud1.csv using 2D imshow
python twoD.py
# plot ./data/dotCloud2.csv using 2D imshow
python twoD.py f2

# plot full dotCloud.csv using voxel
python threeD.py
# plot 0:00:00 - 0:05:00 data
python threeD.py 0:05:00
# plot 0:00:00 - 0:05:00 and slice since the 5th cubes
python threeD.py 0:05:00 5
# plot 0:00:00 - 0:05:00 and slice since the 5th cubes with legend on
python threeD.py 0:05:00 5 on
```

### for lazy guy to start
1. install [anaconda](https://www.anaconda.com/distribution/)
2. start plotting...
```
python twoD.py
python threeD.py
```

### benchmark(without plotting)
Test Hardware information:
  - core: 2.2 GHz Intel Core i7
  - memory: 16 GB 1600 MHz DDR3

| x,y,z-cubes | time cost by seconds |
| -- | -- |
| (100, 500, 100) | 8.3 |
| (100, 1000, 100) | 14.51 |
| (200, 1000, 100) | 31.91 |
| (200, 500, 200) | 28.76 |
| (200, 1000, 200) | 63.25 |
