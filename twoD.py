import numpy as np
import math
import sys
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

DOTCLOUND_FILE1 = './data/dotCloud1.csv'
DOTCLOUND_FILE2 = './data/dotCloud2.csv'
SPECTRUM_FILE = './data/two_d.csv'

def getVec(fileName):
  ret = []
  fid = open(fileName, 'r')
  lines = fid.readlines()
  col = len(lines[0].split(','))
  for line in lines:
    if len(line) == col:
      continue
    else:
      ret.append(line.strip().split(','))
  return ret

def vec2NyArr(vec):
  r = len(vec)
  c = len(vec[0])
  pic = np.zeros((r,c))
  for (idxRow, row) in enumerate(vec):
    for (idxCol, e) in enumerate(row):
      if e != '':
        pic[idxRow, idxCol] = e
  return pic

def findNearestMidIdx(v, arrow='left'):
  ret = (len(v), -1)
  if arrow == 'left':
    v = v[::-1]
  for (idx, o) in enumerate(v):
    if o != 0:
      return (idx, o)
  return ret

def findEdge(v, arrow='left'):
  if arrow == 'left':
    for (idx, o) in enumerate(v):
      if o != 0:
        return idx
  else:
    v = v[::-1]
    for (idx, o) in enumerate(v):
      if o != 0:
        return len(v)-idx-1

def updateRow(row, arrow='left'):
  if arrow == 'left':
    for (idx, o) in enumerate(row+1):
      if (o == 0 and row[idx-1] != 0):
        row[idx] = row[idx-1]
    return row
  else:
    row = row[::-1]
    for (idx, o) in enumerate(row+1):
      if (o == 0 and row[idx-1] != 0):
        row[idx] = row[idx-1]
    return row[::-1]

def updateThroughColumn(pic):
  z = np.copy(pic)
  [r, c] = z.shape
  for i in range(1,r):
    for j in range(c):
      if z[i, j] == 0 and z[i-1,j] != 0:
        z[i, j] = z[i-1, j]
  return z

def checkArr(arr):
  idxLeft = findEdge(arr)
  idxRight = findEdge(arr, arrow='right')
  for i in range(idxLeft, idxRight):
    if arr[i] == 0:
      return True
  return False

def fill(pic):
  [r, c] = pic.shape
  mid = math.floor(c/2)
  # 1. 先根据column尽量补齐空元素
  pic2 = updateThroughColumn(pic)
 
  # 2. 根据row来补齐空元素
  for i in range(r):
    arr = pic2[i,:]
    if checkArr(arr):
      _leftHalf = pic2[i,:mid+1]
      _rightHalf = pic2[i,mid+1:]
      leftHalf = updateRow(_leftHalf)
      rightHalf = updateRow(_rightHalf,'right')
      pic2[i,:] = np.concatenate((leftHalf, rightHalf))
  return pic2

def readDotCloud(fileName):
  vec = getVec(fileName)
  arr = vec2NyArr(vec)
  filledArr = fill(arr)
  return filledArr

def readSpectrum():
  fid = open(SPECTRUM_FILE, 'r')
  lines = fid.readlines()
  # 因为点云图只有6层，频谱仪数据暂时选择6行
  r = len(lines)
  c = 5
  pic = np.zeros((r,c))
  cnt = 0
  ret = []
  for line in lines:
    content = line.strip().split(',')
    pic[cnt, :] = [content[4], content[5],content[6],content[7],content[8]]
    cnt += 1
  return pic

def gen3dData(pic):
  [r, c] = pic.shape
  print(pic)
  fig = plt.figure()
  ax = fig.gca(projection='3d')
  dim = 100
  X = range(0, c)
  Y = range(0, dim)
  Z = np.zeros((dim,len(X)))
  X, Y = np.meshgrid(X,Y)
  for i in range(30):
    Z[i,:] = pic[i,:]

  surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
  fig.colorbar(surf, shrink=0.5, aspect=5)
  plt.show()

def plot3D(spectrumArr, data):
  im = plt.imshow(data, interpolation='none')
  mean = np.mean(spectrumArr[1:7,:], axis=0)
  values = np.unique(data.ravel())
  # get the colors of the values, according to the 
  # colormap used by imshow
  colors = [ im.cmap(im.norm(value)) for value in values]
  
  # create a patch (proxy artist) for every color 
  patches = [ mpatches.Patch(color=colors[i], label="M1 {}%, M2 {}%, M3 {}% M4 {}% M5 {}%".format(
    round(spectrumArr[i,0],2),
    round(spectrumArr[i,1],2),
    round(spectrumArr[i,2],2),
    round(spectrumArr[i,3],2),
    round(spectrumArr[i,4],2),
    ),) for i in range(1,len(values)) ]
  patches.append(mpatches.Patch(color='black', label="ALL M1 {}%, M2 {}%, M3 {}% M4 {}% M5 {}".format(
    round(mean[0],2),
    round(mean[1],2),
    round(mean[2],2),
    round(mean[3],2),
    round(mean[4],2),
    ),))
  # document for legent location https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.legend.html
  plt.legend(handles=patches, bbox_to_anchor=(0, -0.3), loc=2, borderaxespad=0. )
  plt.xlabel('Width by decimetre')
  plt.ylabel('Height by decimetre')
  plt.title('Dot Cloud 2D Demo')
  plt.show()

def plot2D(spectrumArr, data):
  im = plt.imshow(data, interpolation='none')
  values = np.unique(data.ravel())
  values = values[1:]
  mean = np.mean(spectrumArr[0:len(values),:], axis=0)
  # get the colors of the values, according to the 
  # colormap used by imshow
  colors = [ im.cmap(im.norm(value)) for value in values]
  
  # create a patch (proxy artist) for every color 
  patches = [ mpatches.Patch(color=colors[i], label="M1 {}%, M2 {}%, M3 {}% M4 {}% M5 {}%".format(
    round(spectrumArr[i,0],2),
    round(spectrumArr[i,1],2),
    round(spectrumArr[i,2],2),
    round(spectrumArr[i,3],2),
    round(spectrumArr[i,4],3),
    ),) for i in range(0,len(values)) ]
  patches.append(mpatches.Patch(color='black', label="ALL M1 {}%, M2 {}%, M3 {}% M4 {}% M5 {}%".format(
    round(mean[0],2),
    round(mean[1],2),
    round(mean[2],2),
    round(mean[3],2),
    round(mean[4],3),
    ),))
  # document for legent location https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.legend.html
  plt.legend(handles=patches, loc='upper left')
  plt.xlabel('Width by decimetre')
  plt.ylabel('Height by decimetre')
  plt.title('Dot Cloud 2D Demo')
  plt.show()

def make_ax(grid=False):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.grid(grid)
    return ax

def main():
  argsNum = len(sys.argv)
  if (argsNum < 2):
    file = DOTCLOUND_FILE1
    time = 0
  elif argsNum == 2 and sys.argv[1] == 'f1':
    file = DOTCLOUND_FILE1
    time = 0
  elif argsNum == 2 and sys.argv[1] == 'f2':
    file = DOTCLOUND_FILE2
    time = 0
  elif argsNum > 2 and sys.argv[1] == 'f1':
    file = DOTCLOUND_FILE1
    time = int(sys.argv[2])-1
  elif argsNum > 2 and sys.argv[1] == 'f2':
    file = DOTCLOUND_FILE2
    time = int(sys.argv[2])-1
  else:
    pass

  # 1. 读取频谱仪数据，只读最早的6分钟数据
  spectrumArr = readSpectrum()
  spectrumArr = spectrumArr[time:,:]

  # 2. 读取点云数据
  dotCloundArr = readDotCloud(file)
  [r, c] = dotCloundArr.shape

  # 绘制二维图片
  plot2D(spectrumArr, dotCloundArr)


if __name__ == '__main__':
   main()
