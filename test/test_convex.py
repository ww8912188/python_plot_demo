import numpy as np
import math
import sys
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay

def findConvexHull(points):
    # 获取convex index
    hull = ConvexHull(points)
    return hull.vertices


def in_hull(p, hull):
    """
    Test if points in `p` are in `hull`

    `p` should be a `NxK` coordinates of `N` points in `K` dimensions
    `hull` is either a scipy.spatial.Delaunay object or the `MxK` array of the 
    coordinates of `M` points in `K`dimensions for which Delaunay triangulation
    will be computed
    """
    if not isinstance(hull, Delaunay):
        hull = Delaunay(hull)

    return hull.find_simplex(p) >= 0

def helper1(dim):
    v = []
    for i in range(dim):
        for j in range(dim):
            v.append([i, j])
    return np.array(v)


a = np.array([
  [1, 0, 0, 0, 0, 1,],
  [0, 0, 0, 0, 0, 0,],
  [0, 0, 0, 0, 0, 0,],
  [0, 0, 0, 0, 0, 0,],
  [0, 0, 0, 0, 0, 0,],
  [1, 0, 0, 1, 0, 0,],
])

dim = 6
indices = np.transpose(np.nonzero(a))
q = helper1(dim)
checker = in_hull(q, indices)
real = np.reshape(checker, (6, 6)).astype(int)

print('before')
print(a)
print('after')
print(real)
